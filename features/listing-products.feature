Feature: List products
  In order to make some errands
  As a customer
  I want to read the list of available products

  Scenario: A customer wants to see the available products
    Given a customer
    And there are some products to sell
    When the customer looks for the available products
    Then he must see all the available products

  Scenario: A customer wants to find rollerskates
    Given a customer
    And there are rollerskates to sell
    When the customer searches for rollerskates
    Then he must see the rollerskates
