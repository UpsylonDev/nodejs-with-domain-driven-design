import test from 'ava';
import { Quantity } from '../src/domain/Quantity';

test('Quantity should not be negative', async (t) => {
  const error = await t.throws(() => {
    new Quantity(-10);
  });
  t.is(error.message, 'Quantity must be possitive');
});

test('quantity should be created', async (t) => {
  const quantity = new Quantity(0);
  t.is(quantity.value, 0);
});
