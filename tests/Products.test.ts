// Reference documentation for AVA's built-in assertions:
// https://github.com/avajs/ava/blob/main/docs/03-assertions.md#built-in-assertions
import test from 'ava';
import { Price } from '../src/domain/Price';
import { Product } from '../src/domain/Product';
import { Quantity } from '../src/domain/Quantity';

test('should create a product', (t) => {
  const price = new Price(1);
  const quantity = new Quantity(10);
  const name = 'Alphabet cubes';
  const id = 'abc';
  const product = new Product(id, name, price, quantity);

  t.is(product.id, id);
  t.is(product.name, name);
  t.is(product.price, price);
  t.is(product.quantity, quantity);
});

test('product should not have empty id', async (t) => {
  const error = await t.throws(() => {
    new Product('', 'name', new Price(1), new Quantity(1));
  });
  t.is(error.message, 'Product id must not be blank');
});

test('product should not have blank id', async (t) => {
  const error = await t.throws(() => {
    new Product('  ', 'name', new Price(1), new Quantity(1));
  });
  t.is(error.message, 'Product id must not be blank');
});

test('product should not have empty name', async (t) => {
  const error = await t.throws(() => {
    new Product('id', '', new Price(1), new Quantity(1));
  });
  t.is(error.message, 'Product name must not be blank');
});

test('product should not have blank name', async (t) => {
  const error = await t.throws(() => {
    new Product('id', '  ', new Price(1), new Quantity(1));
  });
  t.is(error.message, 'Product name must not be blank');
});
