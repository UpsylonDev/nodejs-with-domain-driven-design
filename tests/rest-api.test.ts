import test from 'ava';
import fetch from 'node-fetch';
import { startServer as startExpressServer } from '../src/infra/express-server';
import { startServer as startFastifyServer } from '../src/infra/fastify-server';
import { Product } from '../src/domain/Product';
import { setProducts, findAllProducts } from '../src/stubs/InMemoryProducts';
import { Price } from '../src/domain/Price';
import { Quantity } from '../src/domain/Quantity';
import { api } from '../src/domain/api/API';

test('should fetch all products using Express', async (t) => {
  // Init fake db
  const expectedProductList: Product[] = [
    new Product('id', 'name', new Price(100), new Quantity(10)),
  ];
  setProducts(expectedProductList);
  // Start server
  const port = 3000;
  await startExpressServer(port, api({ findAllProducts }));
  // GET /products
  const url = `http://localhost:${port}`;
  const res = await fetch(`${url}/products`);
  const products = await res.text();
  t.is(products, JSON.stringify(expectedProductList));
});

test('should fetch all products using Fastify', async (t) => {
  // Init fake db
  const expectedProductList: Product[] = [
    new Product('id', 'name', new Price(100), new Quantity(10)),
  ];
  setProducts(expectedProductList);
  // Start server
  const port = 3001;
  await startFastifyServer(port, api({ findAllProducts }));
  // GET /products
  const url = `http://localhost:${port}`;
  const res = await fetch(`${url}/products`);
  const products = await res.text();
  t.is(products, JSON.stringify(expectedProductList));
});
