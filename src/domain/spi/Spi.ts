import { Product } from '../Product';

export type FindAllProducts = () => Product[];

export interface SPI {
  findAllProducts: FindAllProducts;
}
