export class Quantity {
  constructor(public readonly value: number) {
    if (value < 0) {
      throw new Error('Quantity must be possitive');
    }
  }
}
