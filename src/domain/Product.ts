import { Price } from './Price';
import { Quantity } from './Quantity';

export class Product {
  constructor(
    public readonly id: string,
    public readonly name: string,
    public readonly price: Price,
    public readonly quantity: Quantity
  ) {
    ['id', 'name'].forEach((key: string) => {
      if ((this[key as keyof Product] as string).trim().length == 0) {
        throw new Error(`Product ${key} must not be blank`);
      }
    });
  }
}
