import { startServer } from './fastify-server';
import { makeAPI } from './CompositionRoot';

const PORT = process.env.PORT || '3000';
startServer(parseInt(PORT), makeAPI())
  .then(() => console.log(`http://localhost:${PORT}/products is ready`)) // eslint-disable-line no-console
  .catch((err) => console.error(err)); // eslint-disable-line no-console
