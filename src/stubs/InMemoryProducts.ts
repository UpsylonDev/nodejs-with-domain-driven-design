import { Price } from '../domain/Price';
import { Product } from '../domain/Product';
import { Quantity } from '../domain/Quantity';
import { FindAllProducts } from '../domain/spi/Spi';

let allProducts = [
  new Product('rs1', 'Rollerskates', new Price(150), new Quantity(50)),
  new Product('sk1', 'Skateboard', new Price(200), new Quantity(10)),
];

export const findAllProducts: FindAllProducts = () => allProducts;

export const setProducts = (products: Product[]) => {
  allProducts = products;
};
