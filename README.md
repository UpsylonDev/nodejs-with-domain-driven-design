![Node.js CI](https://github.com/adrienjoly/craft-with-typescript-template/workflows/Node.js%20CI/badge.svg)

# Domain Driven Design with Node.js and TypeScript

Developed during a live-coding session streamed on Twitch, this repository explores the way to apply [BDD](https://dannorth.net/introducing-bdd/), [DDD](https://www.infoq.com/fr/minibooks/domain-driven-design-quickly/) and [Hexagonal Architecture](https://beyondxscratch.com/2017/08/19/hexagonal-architecture-the-practical-guide-for-a-clean-architecture/) while implementing and testing a (very) simple web application in Node.js and TypeScript.

It's based on the following template: [adrienjoly/craft-with-typescript-template: A Software Crafter template to write and test clean TypeScript code with Visual Studio Code, BDD style.](https://github.com/adrienjoly/craft-with-typescript-template).

Video recordings of our Twitch live-coding sessions:

- [Développer en NodeJS avec le Domain-Driven Design : Le Domaine - YouTube](https://www.youtube.com/watch?v=01G9YqAOQFg)
- [Développer en NodeJS avec l'Architecture Hexagonale - YouTube](https://www.youtube.com/watch?v=VMgFozpPSkM)

## How to run tests

```sh
$ nvm use # to use the version of node.js specified in .nvmrc
$ npm install
$ npm test # or, in vscode, press Ctrl-F5
```

## How to start the server

After running the commands listed above:

```sh
$ npm start
```

## Documentation on how to write tests with Cucumber-js

- [Todd Anderson - BDD in JavaScript: CucumberJS](https://custardbelly.com/blog/blog-posts/2014/01/08/bdd-in-js-cucumberjs/index.html)
- [Todd Anderson - BDD in JavaScript II: CucumberJS, the World and Background](https://www.custardbelly.com/blog/blog-posts/2014/01/22/cucumberjs-world/index.html)
- [cucumber-js/nodejs_example.md at master · cucumber/cucumber-js](https://github.com/cucumber/cucumber-js/blob/master/docs/nodejs_example.md)
- [cucumber-js/step_definitions.md at master · cucumber/cucumber-js](https://github.com/cucumber/cucumber-js/blob/master/docs/support_files/step_definitions.md)
- [cucumber-js/api_reference.md at master · cucumber/cucumber-js](https://github.com/cucumber/cucumber-js/blob/master/docs/support_files/api_reference.md)
- [Cucumber Cheatsheet](https://gist.github.com/yuriiik/5728701)

## Planning du stream

1. Intro: présentation du sujet, plan et présentation synthétique du vocabulaire TDD (agrégat, etc...)
2. Écriture de 2-3 scenarios
3. Définition de données de test: 3-4 produits exprimés en JSON
4. Implémentation des step definitions
5. Implémentation de la feature en ATDD
6. Ajouter une API REST
7. Ajouter une SPI pour persister les données
