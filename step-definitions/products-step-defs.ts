import { Given, When, Then, Before } from '@cucumber/cucumber';
import { expect } from 'chai';
import { api } from '../src/domain/api/API';
import { Price } from '../src/domain/Price';
import { Product } from '../src/domain/Product';
import { Quantity } from '../src/domain/Quantity';
import { setProducts, findAllProducts } from '../src/stubs/InMemoryProducts';

// Glue code / steps
Before(function () {
  // we plug our Domain to a fake DB
  const { listProducts, searchProducts } = api({ findAllProducts });
  this.listProducts = listProducts;
  this.searchProducts = searchProducts;
});

Given('a customer', function () {});

Given('there are some products to sell', async function () {
  this.availableProducts = [
    new Product('rs1', 'Rollerskates', new Price(150), new Quantity(50)),
    new Product('sk1', 'Skateboard', new Price(200), new Quantity(10)),
  ];
  setProducts(this.availableProducts);
});

Given('there are {word} to sell', async function (productName: string) {
  this.availableProducts = [
    new Product('rs1', productName, new Price(150), new Quantity(50)),
    new Product('sk1', 'Skateboard', new Price(200), new Quantity(10)),
  ];
  setProducts(this.availableProducts);
});

When('the customer looks for the available products', async function () {
  this.productResults = this.listProducts();
});

When('the customer searches for {word}', async function (productName: string) {
  this.productResults = this.searchProducts(productName);
});

Then('he must see all the available products', async function () {
  expect(this.productResults).to.equal(this.availableProducts);
});

Then('he must see the {word}', async function (productName: string) {
  expect(
    this.productResults.some((product: Product) => product.name === productName)
  ).to.equal(true);
});
